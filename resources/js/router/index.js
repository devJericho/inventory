import { createRouter, createWebHistory } from "vue-router";
import ExampleComponent from "../components/ExampleComponent.vue";
import Login from "../auth/Login.vue";

const routes = [
    {
        path: '/',
        component: Login
    },
    {
        path: '/dashboard',
        component: ExampleComponent 
    },
];

const router = createRouter({
    history: createWebHistory(),
    routes
});

export default router;